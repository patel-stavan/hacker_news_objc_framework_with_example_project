//
//  HNApiExposer.h
//  hackerNewsSdkForCordova
//
//  Created by Stavan Patel on 5/21/17.
//
//

#import <Cordova/CDV.h>
@import HackerNewsSDKFramework;

@interface HNApiExposer : CDVPlugin

- (void) cordovaInitSDK:(CDVInvokedUrlCommand *)command;
- (void) cordovaShowHackerNews:(CDVInvokedUrlCommand *)command;


#pragma mark - Native Methods for Cordova Methods.
// Pure native code to persist data
- (void) initSDK;
    
// Native code to show viewcontroller with all the information.
- (void) showHackerNews;


@end
