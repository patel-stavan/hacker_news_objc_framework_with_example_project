//
//  HNApiExposer.m
//  hackerNewsSdkForCordova
//
//  Created by Stavan Patel on 5/21/17.
//
//

#import "HNApiExposer.h"

@interface HNApiExposer()
@property(nonatomic,strong) hackernewsSDK * hackerNewsSdk;
@end

@implementation HNApiExposer
    
-(void)cordovaInitSDK:(CDVInvokedUrlCommand *)command {
    NSLog(@"command received - cordova - initSDK");
    
    [self initSDK];
    
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)initSDK {
    self.hackerNewsSdk = [[hackernewsSDK alloc]initSDK];
}
    
-(void)cordovaShowHackerNews:(CDVInvokedUrlCommand *)command {
    NSLog(@"command received - cordova - showHackerNews");
    CDVPluginResult *pluginResult;
    if(self.hackerNewsSdk) {
        [self showHackerNews];
         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}
    
-(void)showHackerNews{
    [self.hackerNewsSdk showHackerNews];
}

@end
