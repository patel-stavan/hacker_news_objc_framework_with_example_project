//
//  hackernewsSDK.h
//  hackernewsSDK
//
//  Created by Stavan Patel on 5/16/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface hackernewsSDK : NSObject

-(id)initSDK;
-(id)showHackerNews;
-(void)deallocHackerNewsSdk;

@end
