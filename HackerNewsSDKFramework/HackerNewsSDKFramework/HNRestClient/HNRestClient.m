//
//  HNRestClient.m
//  HackerNewsSDKFramework
//
//  Created by Stavan Patel on 5/18/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

#import "HNRestClient.h"
#import "AFNetworking.h"

static NSString * const kItemRequestString = @"https://hacker-news.firebaseio.com/v0/item/%ld.json";
static NSString * const kTopStoriesRequestString = @"https://hacker-news.firebaseio.com/v0/topstories.json";

@interface HNRestClient()

@property(nonatomic,strong) NSURLSessionConfiguration *configuration;
@property(nonatomic,strong) AFURLSessionManager * sessionManager;

@end


@implementation HNRestClient


-(id)init {
    self = [super init];
    if(self){
        self.configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.sessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:self.configuration];
        dispatch_queue_t queue = dispatch_queue_create("com.stavan.patel.hackernewssdk", DISPATCH_QUEUE_CONCURRENT);
        dispatch_group_t group = dispatch_group_create();
        self.sessionManager.completionQueue = queue;
        self.sessionManager.completionGroup = group;
    }
    return self;
}


-(NSMutableURLRequest *)makeItemRequestForId:(NSInteger)id{
    
    //    NSString *URLString = @"https://hacker-news.firebaseio.com/v0/item/14361425.json";
    
    NSString *urlString = [NSString stringWithFormat:kItemRequestString,(long)id];
    
    NSMutableURLRequest * request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET"
                                                                                  URLString:urlString
                                                                                 parameters:nil
                                                                                      error:nil];
    
    
    
    return request;
}


-(void)fetchTopStoriesWithSuccess:(void (^)(NSArray<NSNumber*>* topIds))idsblock{
    
    
    NSMutableURLRequest * request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET"
                                                                                  URLString:kTopStoriesRequestString
                                                                                 parameters:nil
                                                                                      error:nil];
    
    NSURLSessionDataTask *dataTask = [self.sessionManager dataTaskWithRequest:request
                                                            completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                                                if (error) {
                                                                    NSLog(@"Error: %@", error);
                                                                } else {
                                                                    NSLog(@"%@", responseObject);
                                                                    NSArray<NSNumber *> * ids = responseObject;
                                                                    idsblock(ids);
                                                                }
                                                            }];
    
    [dataTask resume];
    

}


-(void)processItemRequests:(NSArray<NSMutableURLRequest *> *)requests
  withCompletionBlock:(nullable void (^)(NSArray<HNItem*> *item))getItemHandler
{
    
    NSMutableArray<HNItem *> * items = [[NSMutableArray alloc]init];
    
    
    for(NSMutableURLRequest* request in requests) {
        dispatch_group_enter(self.sessionManager.completionGroup);
        NSURLSessionDataTask *dataTask = [self.sessionManager dataTaskWithRequest:request
                                                                completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                                                                    if (error) {
                                                                        dispatch_group_leave(self.sessionManager.completionGroup);
                                                                        NSLog(@"Error: %@", error);
                                                                    } else {
//                                                                        NSLog(@"%@", responseObject);
                                                                        HNItem * item = [[HNItem alloc]initFromDictionary:responseObject];
                                                                        NSLog(@"item created");
                                                                        [items addObject:item];
                                                                        dispatch_group_leave(self.sessionManager.completionGroup);
                                                                    }
                                                                }];
        [dataTask resume];
    }
    
    dispatch_group_notify(self.sessionManager.completionGroup, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSLog(@"finally!");
        getItemHandler(items);
    });

    
    

}


@end
