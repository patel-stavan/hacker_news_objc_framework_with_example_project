//
//  HNResponseInitProtocol.h
//  HackerNewsSDKFramework
//
//  Created by Stavan Patel on 5/18/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HNResponseInitProtocol <NSObject>

-(instancetype)initFromDictionary:(NSDictionary *)dictionary;

@end
