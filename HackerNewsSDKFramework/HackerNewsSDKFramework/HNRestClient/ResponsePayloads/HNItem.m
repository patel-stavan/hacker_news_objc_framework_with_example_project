//
//  HNItem.m
//  HackerNewsSDKFramework
//
//  Created by Stavan Patel on 5/18/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

#import "HNItem.h"

@implementation HNItem

-(instancetype)initFromDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if(self) {
        self.userName = [dictionary objectForKey:@"by"];
        self.descendents = [dictionary objectForKey:@"descendants"];
        self.itemId = [dictionary objectForKey:@"id"];
        self.kidItemIds = [dictionary objectForKey:@"kids"];
        self.score = [dictionary objectForKey:@"score"];
        self.title = [dictionary objectForKey:@"title"];
        self.url = [dictionary objectForKey:@"url"];
        self.time = [NSDate dateWithTimeIntervalSince1970:[(NSNumber*)[dictionary objectForKey:@"time"]doubleValue]];
    }
    return self;
}

-(id)init{
    NSLog(@"not implemented error");
    //TODO: throw NSException. So that it crashes and is resolved by the dev when developing
    
    return nil;
}

@end
