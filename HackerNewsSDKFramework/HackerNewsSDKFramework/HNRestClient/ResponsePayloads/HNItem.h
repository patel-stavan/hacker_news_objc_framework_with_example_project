//
//  HNItem.h
//  HackerNewsSDKFramework
//
//  Created by Stavan Patel on 5/18/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

/** An example of JSON decoded by this class.
 by = JOfferijns;
 descendants = 210;
 id = 14361425;
 kids =     (
 14361505,
 14361688,
 14361583,
 14362211,
 14362090,
 14362065,
 14362255,
 14361589,
 14362038,
 14361837,
 14363054,
 14362291,
 14365920,
 14362534,
 14361956,
 14362394,
 14364384,
 14361532,
 14361599,
 14361786,
 14364312,
 14361652,
 14361922,
 14362719,
 14362592,
 14364837,
 14363007,
 14363180,
 14361903,
 14361593,
 14365986,
 14364542,
 14361940,
 14365836,
 14361878,
 14363468,
 14361560
 );
 score = 595;
 time = 1495046094;
 title = "Android now supports Kotlin";
 type = story;
 url = "https://venturebeat.com/2017/05/17/android-now-supports-the-kotlin-programming-language/";
 */



#import <Foundation/Foundation.h>
#import "HNResponseInitProtocol.h"



@interface HNItem : NSObject <HNResponseInitProtocol>

@property(nonatomic,strong) NSString * userName;
@property(nonatomic) NSNumber * descendents;
@property(nonatomic) NSNumber * itemId;
@property(nonatomic, strong) NSArray<NSNumber *> *kidItemIds;
@property(nonatomic) NSNumber * score;
@property(nonatomic) NSString * title;
@property(nonatomic) NSString * url;
@property(nonatomic) NSDate * time;
@end
