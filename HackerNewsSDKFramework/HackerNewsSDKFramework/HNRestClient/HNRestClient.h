//
//  HNRestClient.h
//  HackerNewsSDKFramework
//
//  Created by Stavan Patel on 5/18/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HNItem.h"

@interface HNRestClient : NSObject

-(NSMutableURLRequest *_Nonnull)makeItemRequestForId:(NSInteger)id;
-(void)processItemRequests:(NSArray<NSMutableURLRequest *> *_Nonnull)requests
       withCompletionBlock:(nullable void (^)(NSArray<HNItem*> * _Nullable item))getItemHandler;
-(void)fetchTopStoriesWithSuccess:(nullable void (^)(NSArray<NSNumber*>* _Nullable topIds))idsblock;
@end
