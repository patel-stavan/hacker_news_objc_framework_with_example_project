//
//  HNDetailWebViewController.h
//  HackerNewsSDKFramework
//
//  Created by Stavan Patel on 5/19/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HNDetailWebViewController : UIViewController

@property(nonatomic,weak) IBOutlet UIWebView * webView;
@property(nonatomic,strong) NSString * urlString;

@end
