//
//  HNDetailWebViewController.m
//  HackerNewsSDKFramework
//
//  Created by Stavan Patel on 5/19/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

#import "HNDetailWebViewController.h"

@interface HNDetailWebViewController ()

@end

@implementation HNDetailWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self showPageForUrl:self.urlString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showPageForUrl:(NSString *)urlString{
    NSURL *url = [NSURL URLWithString:urlString];
    self.webView.delegate = self;
    NSURLRequest *requestObject = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObject];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Error : %@",error);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
