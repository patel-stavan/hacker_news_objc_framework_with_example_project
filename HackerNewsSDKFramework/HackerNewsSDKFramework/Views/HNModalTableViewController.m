//
//  HNModalTableViewController.m
//  HackerNewsSDKFramework
//
//  Created by Stavan Patel on 5/19/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

#import "HNModalTableViewController.h"
#import "HNDetailWebViewController.h"

static NSString * kItemsRetreived = @"HNItemsRetrieved";

@interface HNModalTableViewController ()

@end

@implementation HNModalTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(receivedNotification:)
                                                name:kItemsRetreived
                                              object:nil];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(deallocHackerNewsSdk)];
    [[self navigationItem] setLeftBarButtonItem:cancelButton];

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//     self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self registerNib];
    self.tableView.tableFooterView =[[UIView alloc]initWithFrame:CGRectZero];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.datasource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HNCell"
                                                            forIndexPath:indexPath];
    
    // Configure the cell...
    [self configureCell:cell withItem:[self.datasource objectAtIndex:indexPath.row]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

#pragma mark - tableView delegate

    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    HNDetailWebViewController *detailViewController = [[HNDetailWebViewController alloc]
                                                       initWithNibName:@"HNDetailWebViewController"
                                                       bundle:[NSBundle bundleForClass:[self class]]];
    
    detailViewController.urlString = [self.datasource objectAtIndex:indexPath.row].url;
    
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}


#pragma mark - Helper methods
-(void)registerNib {
    [self.tableView registerNib:[UINib nibWithNibName:@"HNTableViewCell"
                                               bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:@"HNCell"];
}

-(void)configureCell:(UITableViewCell *)cell withItem:(HNItem*)item {
    
    UILabel *titleLabel = [cell viewWithTag:10];
    UILabel *authorLabel = [cell viewWithTag:20];
    UILabel *commentLabel = [cell viewWithTag:30];
    UILabel *scoreLabel = [cell viewWithTag:40];
    UILabel *dateLabel = [cell viewWithTag:50];
    
    
    titleLabel.text = item.title;
    authorLabel.text  = item.userName;
    scoreLabel.text = [item.score description];
    commentLabel.text = [NSString stringWithFormat:@"%ld ",[item.kidItemIds count]] ;
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *formattedDateString = [formatter stringFromDate:item.time];
    
    dateLabel.text = formattedDateString;
}

#pragma mark - Notification Updates
-(void)receivedNotification:(NSNotification *)aNotification {
    NSLog(@"received notification");
    self.datasource = aNotification.object;
    [self.tableView reloadData];
}

#pragma mark - other target action calls
-(void)deallocHackerNewsSdk {
    NSLog(@"dealloc HackerNewsSdk called because cancel pressed on tableview with HNItems screen");
    [self.hackerNewsInstance deallocHackerNewsSdk];
}


/*

*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
