//
//  HNModalTableViewController.h
//  HackerNewsSDKFramework
//
//  Created by Stavan Patel on 5/19/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HNItem.h"
#import "hackernewsSDK.h"

@interface HNModalTableViewController : UITableViewController

@property(nonatomic,strong) NSArray<HNItem *>* datasource;
@property(nonatomic,strong) hackernewsSDK * hackerNewsInstance;

@end
