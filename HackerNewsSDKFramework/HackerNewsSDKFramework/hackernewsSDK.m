//
//  hackernewsSDK.m
//  hackernewsSDK
//
//  Created by Stavan Patel on 5/16/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

#import "hackernewsSDK.h"
#import "HNRestClient.h"
#import <UIKit/UIKit.h>
#import "HNModalTableViewController.h"

static NSString * kItemsRetreived = @"HNItemsRetrieved";

@interface hackernewsSDK ()

@property(nonatomic,strong) UIWindow * modalWindow;
@property(nonatomic,strong) UINavigationController * modalNavigationController;
@property(nonatomic,strong) NSArray<HNItem *> * top20Items;
@property(nonatomic,strong) NSMutableArray<NSMutableURLRequest *> *requests;
@property(nonatomic,strong) hackernewsSDK * instanceSDK;

@end


@implementation hackernewsSDK



-(id)initSDK {
    NSLog(@"From HackerNewsSDK: initSDK: - initing sdk");
    
    __block HNRestClient * restClient = [[HNRestClient alloc]init];
    __weak typeof(self) weakself = self;
    
    NSLog(@"From HackerNewsSDK: initSDK: - fetching stories");
    [restClient fetchTopStoriesWithSuccess:^(NSArray<NSNumber *> * _Nullable topIds) {
        NSLog(@"ids received");
        NSMutableArray * reqArray = [self filterArray:topIds
                                      andMakeRequests:restClient];
        weakself.requests = reqArray;
        NSLog(@"From HackerNewsSDK: initSDK: - requests created. Now Executing requests.");
        [restClient processItemRequests:reqArray withCompletionBlock:^(NSArray<HNItem *> * _Nullable item) {
            NSLog(@"Got all HNItems");
            weakself.top20Items = item;
            [[NSNotificationCenter defaultCenter]postNotificationName:kItemsRetreived
                                                               object:weakself.top20Items];
        }];
    }];
    
    self.instanceSDK = self;
    return self;
}
    
-(NSMutableArray *)filterArray:(NSArray<NSNumber *>*)topIds andMakeRequests:(HNRestClient *)restClient {
    NSMutableArray * reqArray = [[NSMutableArray alloc]init];
    //TODO: add filtering code here if any.
    int numOfItems = 0;
    for(NSNumber * idOfItem in topIds) {
        [reqArray addObject:[restClient makeItemRequestForId:[idOfItem integerValue]]];
        numOfItems = numOfItems+1;
        if(numOfItems == 20) {
            //TODO: break on 20.
            break;
        }
    }
    return reqArray;
}

-(id)showHackerNews {
//    CGFloat width = [[UIScreen mainScreen]bounds].size.width;
//    CGFloat hieght = [[UIScreen mainScreen]bounds].size.height;
//    CGFloat x = width/2;
//    CGFloat y = hieght/2;
//    CGRectMake(x-width/3, y-hieght/3, 2*width/3, 2*hieght/3);
    
    self.modalWindow = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.modalWindow.windowLevel = UIWindowLevelNormal;
    self.modalWindow.backgroundColor = [UIColor redColor];
    self.modalWindow.hidden = NO;
    [self.modalWindow makeKeyAndVisible];
    
    HNModalTableViewController *detailController = [[HNModalTableViewController alloc]
                                                    initWithNibName:@"HNModalTableViewController"
                                                    bundle:[NSBundle bundleForClass:[self class]]];
    
    detailController.datasource = self.top20Items;
    detailController.hackerNewsInstance = self;
    
    self.modalNavigationController=[[UINavigationController alloc]initWithRootViewController:detailController];
//    [self.modalNavigationController setNavigationBarHidden:YES];
    self.modalWindow.rootViewController = self.modalNavigationController;
    
    return nil;
}
    
-(void)deallocHackerNewsSdk{
    self.top20Items = nil;
    self.requests = nil;
    self.modalWindow = nil;
    self.modalNavigationController = nil;
    [[UIApplication sharedApplication].delegate.window makeKeyAndVisible];
}

#pragma mark - finding the topmost view controller to show the modal view from.

- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController;
        return [self topViewController:[navigationController.viewControllers lastObject]];
    }
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabController = (UITabBarController *)rootViewController;
        return [self topViewController:tabController.selectedViewController];
    }
    if (rootViewController.presentedViewController) {
        return [self topViewController:rootViewController];
    }
    return rootViewController;
}

@end
