# README #

Hacker News SDK challenge based on requirements posted at https://gist.github.com/sraj/3766a23e41f47fdd712f66f852603cc2

### Gist of Submission ###

* Base Framework Implementation is in '**HackerNewsSDKFramework**'
* Cordova Plugin Implementation is in '**cordovaExtensionHackerNews**' based on Base Framework above
* Test App implementation based on Base Framework is in '**hackernewsSdkTestApp**'

### TODO/Remaining ###

* **Base Framework** : what criteria to select the top 20 stories from? Right now it just selects the top 20 returned without any criteria. eg. top 20 by score or number of comments etc.

* **Cordova Plugin Implementation** : remaining is to create a plugin.xml file and export the plugin. Right now the plugin is created and coded in a cordova-iOS Application.