//
//  ViewController.m
//  hackernewsSdkTestApp
//
//  Created by Stavan Patel on 5/16/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
@import HackerNewsSDKFramework;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor blueColor];
    
}

-(void)viewDidAppear:(BOOL)animated {
    AppDelegate * delegate = (AppDelegate *)([UIApplication sharedApplication].delegate);
    [delegate.sdkInstance showHackerNews];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
