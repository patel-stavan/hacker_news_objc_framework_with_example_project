//
//  AppDelegate.h
//  hackernewsSdkTestApp
//
//  Created by Stavan Patel on 5/16/17.
//  Copyright © 2017 Stavan Patel. All rights reserved.
//

#import <UIKit/UIKit.h>
@import HackerNewsSDKFramework;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) hackernewsSDK * sdkInstance;

@end

